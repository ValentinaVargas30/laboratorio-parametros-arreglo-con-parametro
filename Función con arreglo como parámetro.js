//Parametro: Variable que la funcion utiliza. Y cuyo
//valor inicial debe "pararse" a la funcion cuando 
//se llama

function mayorN(a){
    let mayor=0;
    for(let i=0;i<a.length;i++){
        if(a[i]>mayor){
            mayor=a[i];
        }

    }

return mayor;
}
